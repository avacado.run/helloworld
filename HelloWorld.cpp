#include <iostream>

void Print()
{
    std::cout << "Salut!\n";
}

int PrintInt(int printNumber)
{
    std::cout << printNumber << "\n";
    return printNumber;
}

int Sum(int a, int b)
{
    return a + b;
}

long long LongSum(long long a, long long b)
{
    return a + b;
}


int main(int argc, char* argv[])
{
    int number = 20;
    
    Print();
    
    std::cout << Sum(5,10) << "\n";
    std::cout << LongSum(596785686785678,186578657865785686) << "\n";

    return 0;
}

